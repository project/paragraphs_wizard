CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Paragraphs Wizard implements a field formatter to render paragraphs in a
multistep wizard providing basic javascript controls for navigation such as
"Next" / "Previous" links.

 * For a full description of the module visit:
   https://www.drupal.org/project/paragraphs_wizard

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/paragraphs_wizard


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * Paragraphs - https://www.drupal.org/project/paragraphs
 * Entity Reference Revisions -
   https://www.drupal.org/project/entity_reference_revisions

INSTALLATION
------------

 * Install the Paragraphs Wizard module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module and its core
       dependencies.
    2. Navigate to Administration > Structure > Paragraphs and create a standard
       paragraph type and add a paragraph field.
    3. Navigate to the Manage display tab and edit the format of the desired
       field to "Paragraph wizard". Select a view mode and Save.

MAINTAINERS
-----------

 * Raffaele Chiocca (rafuel92) - https://www.drupal.org/u/rafuel92

Supporting organization:

 * Ibuildings - https://www.drupal.org/ibuildings
